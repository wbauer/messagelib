/*
   SPDX-FileCopyrightText: 2016-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef RICEENCODINGDECODER_H
#define RICEENCODINGDECODER_H

#include "webengineviewer_private_export.h"
#include "updatedatabaseinfo.h"
#include <QVector>
namespace WebEngineViewer {
//https://developers.google.com/safe-browsing/v4/compression
class RiceDecoder
{
public:
    RiceDecoder(int riceParameter, int numberEntries, const QByteArray &encodingData);
    ~RiceDecoder();

    bool hasOtherEntries() const;
    bool nextValue(uint32_t *value);
    bool nextBits(unsigned int num_requested_bits, uint32_t *x);
    uint32_t bitsFromCurrentWord(unsigned int num_requested_bits);
    bool nextWord(uint32_t *word);
private:
    QByteArray mEncodingData;
    int mRiceParameter;
    int mNumberEntries;

    // Represents how many total bytes have we read from |data_| into
    // |mCurrentWord|.
    int mDataByteIndex;

    // Represents the number of bits that we have read from |mCurrentWord|. When
    // this becomes 32, which is the size of |mCurrentWord|, a new
    // |mCurrentWord| needs to be read from |data_|.
    unsigned int mCurrentWordBitIndex;

    // The 32-bit value read from |data_|. All bit reading operations operate on
    // |mCurrentWord|.
    uint32_t mCurrentWord;
};

class WEBENGINEVIEWER_TESTS_EXPORT RiceEncodingDecoder
{
public:
    RiceEncodingDecoder();
    ~RiceEncodingDecoder();

    static QVector<quint32> decodeRiceIndiceDelta(const WebEngineViewer::RiceDeltaEncoding &riceDeltaEncoding);
    static QVector<quint32> decodeRiceHashesDelta(const WebEngineViewer::RiceDeltaEncoding &riceDeltaEncoding);
};
}

#endif // RICEENCODINGDECODER_H
