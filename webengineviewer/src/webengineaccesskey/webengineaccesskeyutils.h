/*
   SPDX-FileCopyrightText: 2016-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/
#ifndef MAILWEBENGINEACCESSKEYUTILS_H
#define MAILWEBENGINEACCESSKEYUTILS_H

#include <QString>

namespace WebEngineViewer {
namespace WebEngineAccessKeyUtils {
Q_REQUIRED_RESULT QString script();
}
}

#endif // MAILWEBENGINEACCESSKEYUTILS_H
