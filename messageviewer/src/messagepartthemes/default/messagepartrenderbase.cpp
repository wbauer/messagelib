/*
    SPDX-FileCopyrightText: 2017 Sandro Knauß <sknauss@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later WITH Qt-Commercial-exception-1.0
*/

#include "messagepartrendererbase.h"

using namespace MessageViewer;

RenderContext::~RenderContext() = default;

MessagePartRendererBase::MessagePartRendererBase()
{
}

MessagePartRendererBase::~MessagePartRendererBase()
{
}
