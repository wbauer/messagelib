/*
    This file is part of KMail's plugin interface.
    SPDX-FileCopyrightText: 2004 Marc Mutz <mutz@kde.org>
    SPDX-FileCopyrightText: 2004 Ingo Kloecker <kloecker@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later WITH Qt-Commercial-exception-1.0
*/

#include "bodypart.h"

MimeTreeParser::Interface::BodyPartMemento::~BodyPartMemento()
{
}

MimeTreeParser::Interface::BodyPart::~BodyPart()
{
}
