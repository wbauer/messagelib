/*
  SPDX-FileCopyrightText: 2014-2020 Laurent Montel <montel@kde.org>

  SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef ATTACHMENTTEMPORARYFILESDIRSTEST_H
#define ATTACHMENTTEMPORARYFILESDIRSTEST_H

#include <QObject>

class AttachmentTemporaryFilesDirsTest : public QObject
{
    Q_OBJECT
public:
    explicit AttachmentTemporaryFilesDirsTest(QObject *parent = nullptr);
    ~AttachmentTemporaryFilesDirsTest();
private Q_SLOTS:
    void shouldHaveDefaultValue();
    void shouldAddTemporaryFiles();
    void shouldAddTemporaryDirs();
    void shouldNotAddSameFiles();
    void shouldNotAddSameDirs();
    void shouldForceRemoveTemporaryDirs();
    void shouldForceRemoveTemporaryFiles();
    void shouldCreateDeleteTemporaryFiles();
    void shouldRemoveTemporaryFilesAfterTime();
};

#endif // ATTACHMENTTEMPORARYFILESDIRSTEST_H
