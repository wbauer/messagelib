/*
  SPDX-FileCopyrightText: 2011-2020 Laurent Montel <montel@kde.org>

  SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef TEMPLATEPARSER_TEMPLATESUTIL_H
#define TEMPLATEPARSER_TEMPLATESUTIL_H

#include "templateparser_export.h"
#include <QString>

namespace TemplateParser {
namespace Util {
TEMPLATEPARSER_EXPORT void deleteTemplate(const QString &id);
TEMPLATEPARSER_EXPORT Q_REQUIRED_RESULT QString getLastNameFromEmail(const QString &str);
TEMPLATEPARSER_EXPORT Q_REQUIRED_RESULT QString getFirstNameFromEmail(const QString &str);
}
}

#endif
