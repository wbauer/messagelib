/*
   SPDX-FileCopyrightText: 2016-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "plugineditorcheckbeforesendconfigurewidget.h"

using namespace MessageComposer;

PluginEditorCheckBeforeSendConfigureWidget::PluginEditorCheckBeforeSendConfigureWidget(QWidget *parent)
    : QWidget(parent)
{
}

PluginEditorCheckBeforeSendConfigureWidget::~PluginEditorCheckBeforeSendConfigureWidget()
{
}

QString PluginEditorCheckBeforeSendConfigureWidget::helpAnchor() const
{
    return QString();
}
