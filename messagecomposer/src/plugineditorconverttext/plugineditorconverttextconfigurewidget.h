/*
   SPDX-FileCopyrightText: 2018-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef PluginEditorConvertTextConfigureWidget_H
#define PluginEditorConvertTextConfigureWidget_H

#include "messagecomposer_export.h"
#include <QWidget>

namespace MessageComposer {
/**
 * @brief The PluginEditorConvertTextConfigureWidget class
 * @author Laurent Montel <montel@kde.org>
 */
class MESSAGECOMPOSER_EXPORT PluginEditorConvertTextConfigureWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PluginEditorConvertTextConfigureWidget(QWidget *parent = nullptr);
    ~PluginEditorConvertTextConfigureWidget();

    virtual void loadSettings() = 0;
    virtual void saveSettings() = 0;
    virtual void resetSettings() = 0;
    virtual QString helpAnchor() const;
Q_SIGNALS:
    void configureChanged();
};
}
#endif // PluginEditorConvertTextConfigureWidget_H
