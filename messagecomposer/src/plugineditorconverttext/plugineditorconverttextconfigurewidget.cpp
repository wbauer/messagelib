/*
   SPDX-FileCopyrightText: 2018-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "plugineditorconverttextconfigurewidget.h"

using namespace MessageComposer;

PluginEditorConvertTextConfigureWidget::PluginEditorConvertTextConfigureWidget(QWidget *parent)
    : QWidget(parent)
{
}

PluginEditorConvertTextConfigureWidget::~PluginEditorConvertTextConfigureWidget()
{
}

QString PluginEditorConvertTextConfigureWidget::helpAnchor() const
{
    return QString();
}
